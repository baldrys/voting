# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-09-26 12:10
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('voting_app', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Vote_for_character',
            new_name='VoteForCharacter',
        ),
    ]
