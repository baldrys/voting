from voting_app.models import *
from django.utils import timezone

def is_active_vote(vote):
    """Определяет активно ли голосование"""
    if vote.end_date < timezone.now() or timezone.now() < vote.start_date:
        return False
    if vote.voteforcharacter_set.latest('votes_number').votes_number >= vote.votes_to_win:
        return False
    return True

def is_completed_vote(vote):
    """Определяет завершено ли голосование"""
    return not (is_active_vote(vote)) and timezone.now() > vote.start_date

def active_votes():
    """Возвращает список активных голосований"""
    list_of_active_votes = []
    for vote in Vote.objects.all():
        vote.vot
        if is_active_vote(vote):
            list_of_active_votes.append(vote)
    return list_of_active_votes

def completed_votes():
    """Возвращает список завершенных голосований"""
    list_of_completed_votes = []
    for vote in Vote.objects.all():
        if is_completed_vote(vote):
            list_of_completed_votes.append(vote)
    return list_of_completed_votes

def get_list_of_votes(type_of_vote="active"):
    """Возвращает список голосований в зависимости от переданного значения"""
    list_of_votes = []
    predicate = is_active_vote if type_of_vote == "active" else is_completed_vote
    for vote in Vote.objects.all():
        if predicate(vote):
            list_of_votes.append(vote)
    return list_of_votes
